import matplotlib.pyplot as plt
import numpy as np
from pylab import *

params = {'axes.labelsize': 14,
     'legend.fontsize': 14,
     'xtick.labelsize': 14,
     'ytick.labelsize': 14}
     
rcParams.update(params)     


filename='../../precice-Solid-watchpoint-west.log'


result = np.genfromtxt(filename, skip_header=1)

timearray = result[:,0]
dataarray = result[:,1:]

dispY = dataarray[:,7]

#timearray = timearray[::10]
#liftforce = liftforce[::10]

def plotSpectrum(y,Fs):
 """
 Plots a Single-Sided Amplitude Spectrum of y(t)
 """
 n = len(y) # length of the signal
 k = arange(n)
 T = n/Fs
 frq = k/T # two sides frequency range
 frq = frq[range(int(n/2))] # one side frequency range

 Y = fft(y)/n # fft computing and normalization
 Y = Y[range(int(n/2))]
 ind = np.argmax(Y)
 print( " Frequency =  %.4f" % frq[ind], " Hz")
 plt.plot(frq, np.abs(Y),'r') # plotting the spectrum
 plt.xlabel('Freq (Hz)')
 plt.ylabel('|Y(freq)|')
 plt.xlim([0.0,2.0])
 plt.grid('on')



sampling=1.0/(timearray[1]-timearray[0])

plt.figure(figsize=(24,8))
plt.subplot(2,1,1)
plt.plot(timearray, dispY, 'b-', linewidth=2.0, markersize=8.0)
plt.xlim([0.0,1000.0])
#plt.ylim([-2.0,2.0])
#axis([0, 10.0, 0.0, 1.0])
plt.title('Vertical displacement')
plt.xlabel('Time')
plt.ylabel('Amplitude')
plt.grid('on')
plt.subplot(2,1,2)
plotSpectrum(dispY, sampling)

savefig('MBD_disp_Re200.png')

plt.show()



