import matplotlib.pyplot as plt
import numpy as np
from pylab import *

params = {'axes.labelsize': 14,
     'legend.fontsize': 14,
     'xtick.labelsize': 14,
     'ytick.labelsize': 14}
     
rcParams.update(params)     


filename='../postProcessing/forces/0/force.dat'


result = np.genfromtxt(filename)

#print(result)


resarray = np.empty((1,result.shape[1]))
resarray[0,:] = result[0,:]


for i in range(1,result.shape[0]):
    if resarray[-1,0] == result[i,0]:
        resarray[-1,:] = result[i,:]
    else:
        resarray = np.vstack((resarray, result[i,:]))


timearray = resarray[:,0]
dataarray = resarray[:,1:]


liftforce = dataarray[:,1]

#timearray = timearray[::10]
#liftforce = liftforce[::10]

def plotSpectrum(y,Fs):
 """
 Plots a Single-Sided Amplitude Spectrum of y(t)
 """
 n = len(y) # length of the signal
 k = arange(n)
 T = n/Fs
 frq = k/T # two sides frequency range
 frq = frq[range(int(n/2))] # one side frequency range

 Y = fft(y)/n # fft computing and normalization
 Y = Y[range(int(n/2))]
 ind = np.argmax(np.abs(Y))
 print(frq[ind])
 print( " Strouhal frequency =  %.4f" % frq[ind], " Hz")
 plt.plot(frq, np.abs(Y),'r') # plotting the spectrum
 plt.xlabel('Freq (Hz)')
 plt.ylabel('|Y(freq)|')
 plt.xlim(0,2.0)
 plt.grid('on')



sampling=1.0/(timearray[1]-timearray[0])

plt.figure(figsize=(20,12))

plt.subplot(2,1,1)
plt.plot(timearray, liftforce, 'b-', linewidth=1.0, markersize=8.0)
#ylim(0,2.0)
#axis([0, 10.0, 0.0, 1.0])
plt.title("Lift Force")
plt.xlabel('Time')
plt.ylabel('Amplitude')
plt.grid('on')
plt.subplot(2,1,2)
plotSpectrum(liftforce, sampling)

plt.savefig('MBD_lift_PS_Re200.png')


plt.show()



