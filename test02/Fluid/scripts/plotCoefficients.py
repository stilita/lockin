import matplotlib.pyplot as plt
import numpy as np
from pylab import *

params = {'axes.labelsize': 14,
     'legend.fontsize': 14,
     'xtick.labelsize': 14,
     'ytick.labelsize': 14}
     
rcParams.update(params)

filename_bench='square-fixed-Re200-forces.dat'

with open(filename_bench) as f:
    lines_after_3 = f.readlines()[3:]

#result1=[item.split("\n")[0] for item in lines_after_3]

timearray = np.zeros(shape(lines_after_3)[0])
dataarray = np.zeros( (shape(lines_after_3)[0],12) )

ind=0
for item in lines_after_3:
    result1 = item.split('\n')[0]
    #print(result1)
    result2 = result1.split('\t')
    #print(result2)
    timearray[ind] = float(result2[0])
    #
    result3=result2[1].replace('(','')
    result4=result3.replace(')','')
    dataarray[ind,:] = result4.split(' ')
    ind = ind+1

FD_bench = dataarray[:,0]+dataarray[:,3]
FL_bench = dataarray[:,1]+dataarray[:,4]




filename='../postProcessing/forces/0/force.dat'

data = np.genfromtxt(filename)


timearray = data[:,0]
dataarray = data[:,1:]


FD = dataarray[:,0]
FL = dataarray[:,1]

rho = 1.0
D   = 1.0
U   = 2.0
factor = rho*D*U**2

CD = 2.0*FD/factor
CL = 2.0*FL/factor

CD_bench = 2.0*FD_bench/factor
CL_bench = 2.0*FL_bench/factor

#print(CD)

#plt.plot(timearray, dragfoce, '-', color='k', linewidth=2.0, markersize=7.0)

plt.figure(figsize=(20,12))
plt.subplot(2,1,1)
plt.plot(timearray, CL, '-', color='b', linewidth=2.0, markersize=7.0, label='simulation')
plt.plot(timearray, CL_bench[1:], '-.', color='k', linewidth=1.0, markersize=7.0, label='benchmark')
plt.xlabel('Time',fontsize=14)
plt.ylabel('$C_L$ ',fontsize=14)

#plt.axis([0, 6, 0, 50])
plt.ylim([-0.8,0.8])
plt.xlim([0.0,200])
plt.grid('on')
plt.legend()

plt.subplot(2,1,2)
plt.plot(timearray, CD, '-', color='r', linewidth=2.0, markersize=7.0, label='simulation')
plt.plot(timearray, CD_bench[1:], '-.', color='k', linewidth=1.0, markersize=7.0, label='benchmark')

plt.xlabel('Time',fontsize=14)
plt.ylabel('$C_D$ ',fontsize=14)

#plt.axis([0, 6, 0, 50])
plt.ylim([1.0,2.0])
plt.xlim([0.0,200])
plt.grid('on')
plt.legend()

#plt.legend(loc='upper left', markerscale=1.0, ncol=1, handlelength = 2.2, numpoints=1, fontsize=12)
#plt.tight_layout()

outfile = 'cl_cd_Re200.png'
plt.savefig(outfile)

plt.show()

