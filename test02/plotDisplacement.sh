#! /bin/bash
gnuplot -p << EOF
set grid
set title 'Displacement of the wing Tip'
set xlabel 'Time [s]'
set ylabel 'Y-Displacement [m]'
# set style line 1 lt 2 lw 6
# set style line 1 lt 2 lw 6
# set linestyle  2 lt 2 lc 1 # red-dashed
# set linestyle  1 lt 2 lc 1 # red-dashed
plot "./precice-Solid-watchpoint-north.log" using 1:9 title "N" with lines, \
     "./precice-Solid-watchpoint-south.log" using 1:9 title "S" with lines, \
     "./precice-Solid-watchpoint-east.log" using 1:9 title 'E' with lines, \
     "./precice-Solid-watchpoint-west.log" using 1:9 title 'W' with lines
EOF

