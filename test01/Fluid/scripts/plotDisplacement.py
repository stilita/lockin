import matplotlib.pyplot as plt
import numpy as np
from pylab import *

params = {'axes.labelsize': 14,
     'legend.fontsize': 14,
     'xtick.labelsize': 14,
     'ytick.labelsize': 14}
     
rcParams.update(params)     

#filename='../postProcessing/sixDoFRigidBodyState/0/sixDoFRigidBodyState-old.dat'
filename='../postProcessing/sixDoFRigidBodyState/0/sixDoFRigidBodyState_0.dat'


data = np.genfromtxt(filename)

timearray = data[:,0]
dataarray = data[:,1:]

dispY = dataarray[:,1]

plt.figure(figsize=(24,8))
plt.plot(timearray, dispY, '-', color='r', linewidth=1.5, markersize=7.0)


plt.xlabel('Time',fontsize=14)
plt.ylabel('Displacement ',fontsize=14)

plt.xlim([0, 1000])
plt.ylim([-2, 2])
plt.grid('on')

#plt.legend(loc='upper left', markerscale=1.0, ncol=1, handlelength = 2.2, numpoints=1, fontsize=12)
#plt.tight_layout()


outfile = 'disp_graph_Re110.png'
plt.savefig(outfile)

plt.show()
