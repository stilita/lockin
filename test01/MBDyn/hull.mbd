# $Header: /var/cvs/mbdyn/mbdyn/mbdyn-1.0/tests/forces/strext/socket/simplerotor/mapping/simplerotor2_mapping,v 1.7 2017/01/12 15:02:57 masarati Exp $
#
# MBDyn (C) is a multibody analysis code. 
# http://www.mbdyn.org
# 
# Copyright (C) 1996-2017
# 
# Pierangelo Masarati	<masarati@aero.polimi.it>
# Paolo Mantegazza	<mantegazza@aero.polimi.it>
# 
# Dipartimento di Ingegneria Aerospaziale - Politecnico di Milano
# via La Masa, 34 - 20156 Milano, Italy
# http://www.aero.polimi.it
# 
# Changing this copyright notice is forbidden.
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation (version 2 of the License).
# 
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Author: Giuseppe Quaranta <quaranta@aero.polimi.it>


begin: data;
	problem: initial value;
end: data;

set: real DT = 1e-2;
set: real INITIAL_TIME = 0.0;
set: real FINAL_TIME = 1000.0;


begin: initial value;

    initial time: INITIAL_TIME;
    final time: FINAL_TIME;
    time step: DT;
#   set: const integer TIMESTEP = 1234;
#   strategy: change, postponed, TIMESTEP; # changes the time step as specified by the drive caller labeled TIMESTEP, whose definition is postponed (this is simply a placeholder)

    method: ms, .6;
    nonlinear solver: newton raphson, modified, 5;
    linear solver: umfpack, colamd, mt, 1;
#   linear solver: naive, colamd;
#    linear solver: umfpack;

    tolerance: 1e-6;
    max iterations: 1000;

    derivatives coefficient: 1e-9;
    derivatives tolerance: 1e-6;
    derivatives max iterations: 100;

    output: iterations;
    output: residual;
end: initial value;


begin: control data;
    structural nodes: 
        +1    # clamped node
        +1    # moving node
    ;
    rigid bodies:
        +1  # mass of other nodes
    ;
    joints:
        +1    # clamp
        +1    # total joint suppressing DoFs
        +1    # linear spring
        +1    # deformable hinge
    ;
    
    forces:
        +1    # external load
    ;
    gravity;
end: control data;

# root reference
set: const integer ROOT = 1;

set: const integer HINGENODE = 2;

set: const real Xroot = 0.0;
set: const real Yroot = 0.0;
set: const real Zroot = 0.0;
reference: ROOT, Xroot, Yroot, Zroot, eye, null, null;



begin: nodes;
    structural: ROOT, static,
        reference, ROOT, null,      # position
        reference, ROOT, eye,       # orientation
        reference, ROOT, null,      # initial velocity
        reference, ROOT, null;      # angular velocity


    structural: HINGENODE, dynamic,
        reference, ROOT, null, 
        reference, ROOT, eye, 
        reference, ROOT, null, 
        reference, ROOT, null;

end: nodes;

#begin: drivers;
#   set: const integer INPUT = 200;
#   file: INPUT, stream,
#         stream drive name, "TS_DRV", # irrelevant but needed
#         create, yes,
#         path, "/tmp/mbdyn.ts.sock", # or use port
#         1;      # one channel: the time step
#     drive caller: TIMESTEP, file, INPUT, 1; # replace placeholder with file driver
#end: drivers;



set: real K = 184.92;
set: real C = 0.35317;

set: real Kr = 50.0;
set: real Cr = 0.1;

set: real m = 117.10;
set: real Ixx = 0.049087;
set: real Iyy = 0.049087;
set: real Izz = 1.0;

set: real da = .005;

begin: elements;
    joint: 500+ROOT, clamp, ROOT, node, node;

    joint: 500+HINGENODE, total joint,
        ROOT,
            position, null,
            position orientation, eye,
            rotation orientation, eye,
        HINGENODE,
            position, null,
            position orientation, eye,
            rotation orientation, eye,

        position constraint, 1, 0, 1, null,

        orientation constraint, 1, 1, 1, null;

    joint: 503, deformable displacement joint,
        ROOT,
            position, null,
        HINGENODE,
            position, null,
        linear viscoelastic isotropic,
            K,
            C;
        # prestrain, single, 0., 0., -1, const, L;
    
    joint: 505, deformable axial joint,
        ROOT,
            position, null,
            orientation, eye,
        HINGENODE,
            position, null,
            orientation, eye,
        linear viscoelastic isotropic,
            Kr,
            Cr;

    # body: BODY_LABEL, NODE_LABEL,
    #    mass
    #    reference node offset
    #    inertia tensor
    
    body: 1001, HINGENODE,
        m,
        null,
        diag, Ixx, Iyy, Izz;

    force: 333, external structural,
        socket,
        create, yes,
            path, "/tmp/mbdyn.node.sock",
            no signal,
            coupling,
            # loose,
            tight,
        #reference node, 1,
        orientation, orientation matrix,
        #use reference node forces, yes,
        1,
            HINGENODE, 

        echo, "ref_config.dat";
        
        gravity: uniform, 0., 0., -1., 0.0;

end: elements;


# vim:ft=mbd
